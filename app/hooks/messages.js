const messagesHooks = {
    before: {
	all: [],
	find: [],
	get: [],
	create: [],
	update: [],
	patch: [],
	remove: [],
    },
    after: {
	all: [],
	find: [],
	create: [],
	update: [],
	patch: [],
	remove:	[],
    }
};

app.service('messages').hooks(messagesHooks)
