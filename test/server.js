const server = require('../bin/www')
let testname = 'test/'+path.basename(__filename)

// Test GET /messages
async function getMessages(){
    let res = await agent.get('/messages')
    res.body.should.be.Array()
}

// Start server
server()

// Mock the browser
let agent = supertest.agent("http://localhost:3000")

// start tests
describe(testname, function(){
    it('should GET /messages', getMessages)
    run()
})


