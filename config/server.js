async function main(){
    app.listen(3000).on('listening', function(){
	console.log('Feathers server listening on localhost:3000')
    });
    await processMessages()
}

module.exports = main
