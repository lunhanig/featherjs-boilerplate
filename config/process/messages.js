async function processMessages(){
    let messageList = await app.service('messages').find();
    if(messageList.length === 0){
	await app.service('messages').create({
	    title: 'Reunião',
	    description: 'Reunião inicial'
	});
	await app.service('messages').create({
	    title: 'Estratégia de desenvolvimento',
	    description: 'Decisão de estratégia de desenvolvimento: linguagens, bibliotecas, ambientes de gerenciamento, etc...'
	});
	messageList = await app.service('messages').find();
    }
    
    console.log('Available messages', messageList);
}
