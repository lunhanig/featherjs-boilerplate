// Creates an Express compatible Feathers application
const app = _feathersjs_express(_feathersjs_feathers());

// Parse HTTP JSON bodies
app.use(_feathersjs_express.json());

// Parse URL-encoded params
app.use(_feathersjs_express.urlencoded({ extended: true }));

// Add REST API support
app.configure(_feathersjs_express.rest());


