"use strict";

// Grunt tasks
const path = require('path');
const each = require('foreach');
const require_from_package = require ('require-from-package');
const check_node = require('check_node');
        
module.exports = function(grunt){

    // Read options property in package.json
    let pkg = grunt.file.readJSON('package.json');
    let grnt = grunt.file.readJSON('Gruntfile.json');
    let options = {
	pkg: {
	    name: pkg.name,
	    version: pkg.version
	},
	concat:grnt.concat
    }
        
    // load prebuild tasks
    grunt.loadNpmTasks('grunt-contrib-concat')
    grunt.loadNpmTasks('grunt-banner');
    
    // local tasks
    grunt.registerTask('build:init', 'An async configure task', function(){
        let done = this.async()
        check_node(function(err, node_path){
            if(err){
		done(err)
            }
	    grunt.config('pkg.node_version', node_path)
            done()
	})
    });

    grunt.registerTask('build:libs', 'An async library maker task', function(){
        let opt = {
            ext: 'js',
	    path: process.cwd(),
	    destination: "boot",
	    pkg: pkg,
	    core: ['fs', 'path', 'http'],
	    validate: function(name){
		let regexp = new RegExp("(grunt.*|check_node|require_from_package|mocha|istanbul|lcov_badge)")
                return !name.match(regexp)
	    }
	}
        require_from_package(opt, this.async())
    })
                
    grunt.initConfig(options)
}
