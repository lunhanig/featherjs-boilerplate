# Feathersjs express server boilerplate

Estrutura de um servidor REST API utilizado com feathers e organizado por pastas, de acordo com os nomes que definem funções específicas deste servidor, cujos arquivos em javascript são concatenados em um arquivo executável único.

## Instalando

    git clone https://www.github.com/lunhg/feathers-boilerplate
    npm install
	
## Build

	mkdir bin
    npm run build

## Testes

    npm test
